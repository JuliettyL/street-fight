import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  // call showModal function
  const winnerImg = createFighterImage(fighter);
  const winnerView = {
    title: `${fighter.name.toUpperCase()} IS A WINNER!`,
    bodyElement:  winnerImg,
    onClose: () => {
      location.reload();
    }
  }
  showModal(winnerView);
}

import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  if(fighter)
  {
    const fighterInfoCard = createElement({ tagName: 'div', className: 'fighter-preview___infoCard' });
    const fighterImage = createFighterImage(fighter);
    const fighterName = createElement({ tagName: 'h2', className: 'fighter-preview___name'});
    const fighterHealth = createElement({tagName: 'p'});
    const fighterAttack = createElement({tagName: 'p'});
    const fighterDefense = createElement({tagName:'p'});
    const fighterHealthIcon = createElement({tagName: 'span'});
    const fighterAttackIcon = createElement({tagName: 'span'});
    const fighterDefenseIcon = createElement({tagName: 'span'});

    fighterHealthIcon.innerHTML = "&#129505; HP: ";
    fighterAttackIcon.innerHTML = "&#128299; ATK: ";
    fighterDefenseIcon.innerHTML = "&#129509; DEF: ";

    fighterName.append(fighter.name);
    fighterHealth.append(fighterHealthIcon, fighter.health);
    fighterAttack.append(fighterAttackIcon, fighter.attack);
    fighterDefense.append(fighterDefenseIcon, fighter.defense);

    fighterInfoCard.append(fighterName, fighterHealth, fighterAttack, fighterDefense);
    fighterElement.append(fighterImage, fighterInfoCard);
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

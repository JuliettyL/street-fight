import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter)
{
  return new Promise((resolve) =>
  {
    const firstFighterHealthIndicator = document.getElementById('left-fighter-indicator');
    const secondFighterHealthIndicator = document.getElementById('right-fighter-indicator');

    const fighterData = {
      currentHealth: 100,
      criticalAttack: false,
      block: false,
      criticalEventKeys: [controls.PlayerOneCriticalHitCombination, controls.PlayerTwoCriticalHitCombination],
    };

    const firstPlayer = { ...firstFighter, ...fighterData, healthIndicator: firstFighterHealthIndicator };
    const secondPlayer = { ...secondFighter, ...fighterData, healthIndicator: secondFighterHealthIndicator };

    function fighterAttack(attacker, defender)
    {
      const totalDamage = getDamage(attacker, defender);

      attacker.block = false;
      defender.currentHealth = defender.currentHealth - totalDamage;

      if (defender.currentHealth < 0)
      {
        defender.currentHealth = 0;
      }

      defender.healthIndicator.style.width = `${defender.currentHealth}%`;

      if (defender.currentHealth <= 0)
      {
        resolve(attacker);
      }
    }

    function criticalHit(fighter) {
      if (!fighter.criticalEventKeys.includes(event.code))
      {
        fighter.criticalEventKeys.push(event.code);
      }

      if (fighter.criticalEventKeys.length === 3)
      {
        fighter.criticalAttack = true;
        fighter.attack = fighter.attack * 2;
        fighter.criticalTime = 0;
        return true;
      }
    }

    function buttonPressed(event)
    {
      if (!event.repeat)
      {
        switch (event.code) {
          case controls.PlayerOneAttack:
          {
            fighterAttack(firstPlayer, secondPlayer);
            break;
          }

          case controls.PlayerTwoAttack:
          {
            fighterAttack(secondPlayer, firstPlayer);
            break;
          }

          case controls.PlayerOneBlock:
          {
            firstPlayer.block = true;
            break;
          }

          case controls.PlayerTwoBlock:
          {
            secondPlayer.block = true;
            break;
          }
        }

        if (controls.PlayerOneCriticalHitCombination.includes(event.code) && criticalHit(firstPlayer))
        {
          fighterAttack(firstPlayer, secondPlayer);
        }

        if (controls.PlayerTwoCriticalHitCombination.includes(event.code) && criticalHit(secondPlayer))
        {
          fighterAttack(secondPlayer, firstPlayer);
        }
      }
    }

    function buttonUp(event)
    {
      switch (event.code) {
        case controls.PlayerOneBlock:
          firstPlayer.block = false;
          break;
        case controls.PlayerTwoBlock:
          secondPlayer.block = false;
          break;
      }

      if (firstPlayer.criticalEventKeys.includes(event.code))
      {
        firstPlayer.criticalEventKeys.splice(firstPlayer.criticalEventKeys.indexOf(event.code), 1);
      }

      if (secondPlayer.criticalEventKeys.includes(event.code))
      {
        secondPlayer.criticalEventKeys.splice(secondPlayer.criticalEventKeys.indexOf(event.code), 1);
      }
    }

    window.addEventListener('keydown', buttonPressed);
    window.addEventListener('keyup', buttonUp);
  });
}
export function getDamage(attacker, defender)
{
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter)
{
  // return hit power
  const criticalHitChance = Math.floor(Math.random() * 2) + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter)
{
  // return block power
  const dodgeChance = Math.floor(Math.random() * 2) + 1;
  return fighter.defense * dodgeChance;
}
